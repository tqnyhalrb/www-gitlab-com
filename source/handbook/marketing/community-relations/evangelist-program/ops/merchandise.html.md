---
layout: markdown_page
title: "Merchandise operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Community Relations manages the merchandise store. This includes:
- adding and removing items to and from the store,
- fulfilling orders,
- maintaining inventory levels and
- responding to store support requests

We are currently using the following vendors:
- Storefront:
  - Shopify
  - Amazon (trial)
- Inventory:
  - Printfection
  - Stickermule

## Access data

- URL: https://shop.gitlab.com
- Login: see 1Password secure note for details

## Operations

### Storefront vendors

#### Shopify

##### Access data

- URL: https://www.shopify.com
- Login: see 1Password secure note for details

##### Adding items

1. [Gather item inventory data ](#gather-item-inventory-data)
1. Log in to Shopify
1. Open the products page
   - Click the Add Product button
   - Fill out information about the item
   - If you don't have the information for the description, please ask/search for it and be careful - the info could be sensitive
   - Fill out the price for the item
   - Fill out the SKU (Stock Keeping Unit) which is the item's ID from Printfection
   - Select "Shopify tracks this product's inventory"
   - Set the quantity according to the Printfection inventory
   - Enter the weight of the product
   - Add the variants of the products if available
   - Before saving the product, please check search engine listing preview

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](https://help.printfection.com/hc/en-us/articles/218014268-Integrating-Printfection-with-Shopify-Using-Zapier-)
{: .alert .alert-info}

##### Removing items

1. Log in to Shopify
1. Open the Products page
   - Click on the product you want to remove
   - Scroll to the bottom of the page where you can find the delete button

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [offical guide](http://shopifynation.com/shopify-tutorials/delete-products-variants-shopify/)
{: .alert .alert-info}

### Amazon

#### Access data

- URL:
- Login: see 1Password secure note for details

## Inventory vendors

### Printfection

#### Access data

- URL: https://www.printfection.com
- Login: see 1Password secure note for details

##### Gather item inventory data

1. Log in to Printfection
1. Open the Inventory
   - Check if the proposed item is available in the inventory
   - Note the item's ID
   - Note the following item details:
     - Name
     - Description
     - Availability
     - Sizes
     - Assets
   - Download the item images

### Stickermule

#### Access data

- URL: https://www.stickermule.com
- Login: see 1Password secure note for details
