$(function() {
  var $regions = $('.reseller-region');
  var $regionItems = $('.reseller-item');
  var $resellerGroups = $('.reseller-group');

  $regions.on('click', function() {
    var $element = $(this);
    var id = $element.attr('id');

    if ($element.hasClass('active')) {
      $regions.removeClass('active');
      $resellerGroups.removeClass('reseller-group-filtered');
      $regionItems.removeClass('zoomIn')
        .fadeOut().promise().done(function() {
          $regionItems.addClass('zoomIn').fadeIn();
        });
    } else {
      $regions.removeClass('active');
      $resellerGroups.addClass('reseller-group-filtered');
      $element.addClass('active');
      $regionItems.removeClass('zoomIn')
        .fadeOut(100).promise().done(function() {
          $regionItems.filter('.region-' + id).addClass('zoomIn').fadeIn(200);
        });
    }
  });
});
